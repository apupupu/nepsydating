import React, { useState } from "react";


export const Form = (props) => {
  const [errorText, setErrorText] = useState(null);

  const onSubmit = (evt) => {
    if (props.onSubmit) props.onSubmit();
    evt.preventDefault();
    jQuery.ajax({
      url: props.action,
      type: props.method || 'POST',
      dataType: 'json',
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content'));
      },
      data: jQuery(`form#${props.id || 'form'}`).serialize(),
    }).then(data => {
      if (props.onSuccess) props.onSuccess(data);
      if (props.afterSubmit) props.afterSubmit();
      setErrorText(null);
    }).catch(data => {
      console.error(data);
      if (props.onError) props.onError(data);
      if (props.afterSubmit) props.afterSubmit();
      setErrorText(data.responseJSON?.message ?? data.statusText);
    });
  }

  return (
    <form
      id={props.id || 'form'}
      method={props.method || 'POST'}
      action={props.action}
      onSubmit={onSubmit}
      className={props.className || 'fancy-form'}
    >
      {errorText
      ? <p style={{ color: 'red' }}>{errorText}</p>
      : null}
      {props.children}
    </form>
  );
}

export const FormSubmit = (props) => {
  return <button type="submit" form={props.formId} className="btn btn-primary">{props.children || 'Submit'}</button>
}
