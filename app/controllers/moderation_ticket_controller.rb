class ModerationTicketController < ApplicationController

  def form_data
    s = ModerationTicketService.new
    data = s.get_form_data()
    return render(:json => data)
  end


  def create
    s = ModerationTicketService.new
    s.create(logged_person_id, params[:title], params[:body], params[:category])
    return render(:json => { :success => true })
  end


  def list
    s = ModerationTicketService.new
    data = s.list()
    return render(:json => data)
  end


  def list_own
    s = ModerationTicketService.new
    data = s.list_own(logged_person_id)
    return render(:json => data)
  end


  def process_ticket
    s = ModerationTicketService.new
    s.create_action(
      logged_person_id,
      params[:moderation_ticket_id],
      params[:assignee],
      params[:status],
      params[:description],
      params[:mod_note],
    )
    return render(:json => { :success => true })
  end


  def action_form_data
    s = ModerationTicketService.new
    data = s.get_action_form_data()
    return render(:json => data)
  end

end
