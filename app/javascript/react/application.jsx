import React from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { LoggedIn } from "./layouts/logged_in";
import { LoggedOut } from "./layouts/logged_out";


const App = (props) => {
  return <BrowserRouter>
    <Routes>
      <Route path='*' element={User.loggedIn ? <LoggedIn/> : <LoggedOut />} />
    </Routes>
  </BrowserRouter>
};

document.addEventListener("DOMContentLoaded", () => {
  const rootEl = document.getElementById("root");
  const root = createRoot(rootEl);
  root.render(<App />);
});
