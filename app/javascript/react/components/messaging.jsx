import React, { useEffect, useState } from "react";

import { dateToStr } from "../../helpers/timeHelpers";
import { Loading } from "./loading";
import { Form, FormSubmit } from "./form";


export const Messaging = (props) => {
  const [messages, setMessages] = useState(null);
  const [sending, setSending] = useState(false);

  const reload = () => {
    jQuery.ajax({
      url: `/messages/list/${props.other_person_id}`,
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setMessages(data);
    });
  }

  const clearMessageInput = () => {
    jQuery('#message_input').val('');
  }

  useEffect(reload, []);

  if (messages === null) return <Loading />;

  return <>
    <Form
      action='/messages/send'
      onSubmit={() => setSending(true)}
      afterSubmit={() => setSending(false)}
      onSuccess={() => {reload(); clearMessageInput()}}
      >
      <input type="hidden" name="recipient_id" value={props.other_person_id} />
      <textarea id='message_input' name='body' />
      <FormSubmit disabled={sending}>Send</FormSubmit>
    </Form>
    <ul>
      {messages.map(m => {
        const timestamp = <span className="me-1">[{dateToStr(new Date(m.created_at))}]</span>;
        const author = <strong className="me-1">{m.sender_name}:</strong>;
        return <li key={m.id}>
          {timestamp}
          {author}
          {m.body}
        </li>;
      })}
    </ul>
  </>;
}
