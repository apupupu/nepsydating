class MessagingService


  # TODO: generate random string id in the client and store it in the db
  # then unique by sender, recipient and str_id to prevent double sends
  # TODO: email notification for new messages, cancel if read before sent (24h or personal preference buffer?), queue max one per sender
  def create(sender_id, recipient_id, body)
    m = Message.create(
      sent_by: sender_id,
      recipient: recipient_id,
      body: body,
    )
  end


  def list(person_id, other_person_id)
    q = <<-SQL
      select m.id,
        m.sent_by,
        m.recipient,
        case when m.deleted then '' else m.body end as body,
        m.created_at,
        m.deleted,
        p.name as sender_name
      from message m
      left join person p
        on p.id = m.sent_by
        and p.deleted = false
      where (
          m.sent_by = :person_id AND m.recipient = :other_person_id
        ) OR (
          m.sent_by = :other_person_id AND m.recipient = :person_id
        )
      order by m.created_at desc
    SQL

    Message.find_by_sql([q, :person_id => person_id, :other_person_id => other_person_id])
  end


  def summary(person_id)
    q = <<-SQL
      select distinct on (c.other_person_id) c.other_person_id,
        p.name,
        c.body as last_message_body,
        c.created_at last_message_at,
        sender.name as last_sender_name
      from (
        (
          select m.recipient as other_person_id, m.sent_by, m.body, m.created_at
          from message m
          where m.sent_by = :person_id AND m.deleted = false
        ) union (
          select m.sent_by as other_person_id, m.sent_by, m.body, m.created_at
          from message m
          where m.recipient = :person_id AND m.deleted = false
        )
      ) c
      inner join person sender on sender.id = c.sent_by
      inner join person p on p.id = c.other_person_id AND p.deleted = false
      left join person_person_link ppl
        on ppl.person_id = :person_id
        and ppl.other_person_id = c.other_person_id
      where (ppl.status is null OR ppl.status != 'blocked')
      order by c.other_person_id, c.created_at desc
    SQL

    return Message.find_by_sql([q, :person_id => person_id])
  end


end