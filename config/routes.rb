Rails.application.routes.draw do
  root 'components#index'

  match '/login', :to => 'login#login', :as => 'login', :via => :post
  match '/register', :to => 'login#register', :as => 'register', :via => :post
  match '/logout', :to => 'login#logout', :as => 'logout', :via => [:get, :post]

  match '/messages/list/:person_id', :to => 'messaging#list', :via => :get
  match '/messages/send', :to => 'messaging#create', :via => :post
  match '/messages/summary', :to => 'messaging#summary', :via => :get

  match '/moderation_ticket/form_data', :to => 'moderation_ticket#form_data', :via => :get
  match '/moderation_ticket/create', :to => 'moderation_ticket#create', :via => :post
  match '/moderation_ticket/list_own', :to => 'moderation_ticket#list_own', :via => :get
  match '/moderation_ticket/list', :to => 'moderation_ticket#list', :via => :get
  match '/moderation_ticket/action_form_data', :to => 'moderation_ticket#action_form_data', :via => :get
  match '/moderation_ticket/process', :to => 'moderation_ticket#process_ticket', :via => :post

  match '/profile/show_own', :to => 'profile#show_own', :via => :get
  match '/profile/browse(/:status)', :to => 'profile#list', :via => :get
  match '/profile/update_status', :to => 'profile#update_status', :via => :post
  match '/profile/show/:profile_id', :to => 'profile#show', :via => :get
  match '/profile/update_own', :to => 'profile#update_own', :via => :post
  match '/profile/get_own_ratings', :to => 'profile#get_own_ratings', :via => :get
  match '/profile/update_own_ratings', :to => 'profile#update_own_ratings', :via => :post

  match '/questions/list', :to => 'question#list', :via => :get
  match '/questions/form_data', :to => 'question#form_data', :via => :get
  match '/questions/create', :to => 'question#create', :via => :post
  match '/questions/update', :to => 'question#update', :via => :post
  match '/questions/delete', :to => 'question#delete', :via => :post
  match '/questions/save_options', :to => 'question#update_options', :via => :post

  match '/question_sets/list', :to => 'question#list_sets', :via => :get
  match '/question_sets/create', :to => 'question#create_set', :via => :post
  match '/question_sets/update', :to => 'question#update_set', :via => :post
  match '/question_sets/delete', :to => 'question#delete_set', :via => :post

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get '*path', to: 'components#index'
end
