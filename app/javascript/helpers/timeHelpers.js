import { format } from "date-fns"


export const dateToStr = (date) => {
  return format(date, 'dd.MM.yyyy HH:mm');
}

