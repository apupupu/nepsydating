import React, { useEffect } from "react";
import * as bootstrap from "bootstrap";


export const Modal = (props) => {
  const id = props.id || 'modal-id';

  onHide = (evt) => {
    if (props.onHide) props.onHide(evt);
  }

  useEffect(() => {
    const elem = document.getElementById(id);
    const modal = new bootstrap.Modal(elem, {backdrop: 'static'});
    modal.show();
    elem.addEventListener('hide.bs.modal', onHide);
    return () => {modal.hide()}
  }, []);  

  return (
    <div id={id} className="modal" tabIndex="-1" role="dialog">
      <div className="modal-dialog modal-lg" role="document">
        <div className="modal-content">
          <div className="modal-header">
            {props.title ?? 'Modal'}
            <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
          </div>
          <div className="modal-body">
            {props.children}
          </div>
          <div className="modal-footer">
            {props.actions ?? <ModalClose/>}
          </div>
        </div>
      </div>
    </div>
  );
}

export const ModalClose = (props) => {
  return (
    <button
      type="button"
      className="btn btn-secondary"
      data-bs-dismiss="modal"
      >
      {props.children ?? 'Close'}
    </button>
  );
}
