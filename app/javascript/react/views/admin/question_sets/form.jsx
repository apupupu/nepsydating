import React, { useEffect, useState } from "react";

import { Button } from "../../../components/button";
import { Form, FormSubmit } from "../../../components/form";
import { Loading } from "../../../components/loading";


export const QuestionSetForm = (props) => {
  const {
    questionSet = null,
    disabled = false,
    onCancel,
    reload,
  } = props;

  const onSuccess = (data) => {
    if (onCancel) onCancel();
    if (!questionSet) {
      jQuery('form#new-question-set-form').trigger('reset');
    }
    if (reload) reload();
  }

  const submitPath = questionSet ? '/question_sets/update' : '/question_sets/create';

  return (
    <Form
      id={questionSet ? `question-set-form-${questionSet.id}` : 'new-question-set-form'}
      action={submitPath}
      onSuccess={onSuccess}
      >
      <input type='hidden' name='id' value={questionSet?.id} />
      <div className="row">
        <div className="col col-12">
          Name
          <input type="text" name="name" disabled={disabled} defaultValue={questionSet?.name} />
        </div>
      </div>
      <div className="row">
        <div className="col col-12">
          Sort order
          <input type="number" name="sort_order" disabled={disabled} defaultValue={questionSet?.sort_order} />
        </div>
        <div className="col col-12">
          Required
          <input type="checkbox" name="required" disabled={disabled} defaultChecked={questionSet?.required} />
        </div>
      </div>
      {onCancel
      ? <Button className='btn btn-secondary me-2' onClick={onCancel}>Cancel</Button>
      : null}
      {disabled ? null : <FormSubmit>{questionSet ? 'Update' : 'Create'}</FormSubmit>}
    </Form>
  )
}

