# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_12_31_201906) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answer_rating", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "person_id", null: false
    t.integer "question_id", null: false
    t.integer "question_option_id"
    t.integer "to_question_option_id"
    t.integer "weight", null: false
    t.json "confidences"
    t.integer "priority", null: false
  end

  create_table "login_attempt", force: :cascade do |t|
    t.integer "login_credentials_id"
    t.string "ip_address"
    t.string "device_name"
    t.boolean "successful", null: false
    t.datetime "created_at", null: false
    t.index ["login_credentials_id"], name: "index_login_attempt_on_login_credentials_id"
  end

  create_table "login_credentials", force: :cascade do |t|
    t.integer "person_id", null: false
    t.string "username"
    t.string "hashed_password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_login_credentials_on_person_id", unique: true
    t.index ["username"], name: "index_login_credentials_on_username", unique: true
  end

  create_table "message", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sent_by"
    t.integer "recipient", null: false
    t.text "body", null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "deleted_at"
    t.datetime "read_at"
  end

  create_table "moderation_ticket", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "created_by", null: false
    t.string "title", null: false
    t.text "body", null: false
    t.integer "moderation_ticket_type_id", null: false
  end

  create_table "moderation_ticket_action", force: :cascade do |t|
    t.integer "moderation_ticket_id", null: false
    t.datetime "created_at", null: false
    t.integer "created_by", null: false
    t.integer "assignee_id"
    t.string "status", null: false
    t.text "description"
    t.text "mod_note"
    t.boolean "current", null: false
  end

  create_table "moderation_ticket_type", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "str_id", limit: 30, null: false
    t.string "name", null: false
    t.integer "sort_order"
    t.boolean "deleted", default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "permission_group_membership", force: :cascade do |t|
    t.string "permission_group_name", null: false
    t.string "member_type", null: false
    t.integer "member_id", null: false
  end

  create_table "person", force: :cascade do |t|
    t.string "name", limit: 60, null: false
    t.string "email"
    t.boolean "deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "user_type_id"
    t.index ["email"], name: "index_person_on_email", unique: true
  end

  create_table "person_person_link", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "person_id", null: false
    t.integer "other_person_id", null: false
    t.string "status"
    t.integer "score"
    t.datetime "score_updated_at"
    t.index ["person_id", "other_person_id"], name: "index_person_person_link_on_person_id_and_other_person_id", unique: true
  end

  create_table "question", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", null: false
    t.string "question_type", null: false
    t.integer "sort_order", null: false
    t.boolean "hard_fact", null: false
    t.boolean "can_private", null: false
    t.boolean "can_restrict", null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "deleted_at"
    t.integer "cloned_options_question_id"
    t.integer "question_set_id"
    t.text "notice"
  end

  create_table "question_answer", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "person_id", null: false
    t.integer "question_id", null: false
    t.integer "question_option_id"
    t.string "text"
    t.string "confidence"
    t.boolean "restricted", null: false
    t.boolean "privated", null: false
  end

  create_table "question_option", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "question_id", null: false
    t.string "name", null: false
    t.integer "sort_order", null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "question_set", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", null: false
    t.integer "sort_order", null: false
    t.boolean "required", null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "user_type", force: :cascade do |t|
    t.string "str_id"
  end

end
