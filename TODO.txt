
- picture upload
- email verification & email notifications
- change the interface of multi_select questions with confidence to have separate rows for every answer (and/or add multi_checkbox question_type)

DONE:
- allow users to elaborate in text on every question
- show new messages on front page

MAYBE LATER:
- allow users to sort questions?
- hideable question sets (by admins for developing of questions, and by users if they don't want to see them)
- invite codes (for alpha testing)
