namespace :permissions do
  task :update => [:environment] do
    # delete old
    PermissionGroupMembership.where(member_type: 'user_type').delete_all

    user = UserType.find_by(str_id: 'user')
    admin = UserType.find_by(str_id: 'admin')

    user_permission_groups = [
      'logged_in',
      'profile',
      'browse_profiles',
      'messaging',
    ]
    admin_permission_groups = [
      'admin',
    ]

    user_permission_groups.each do |pg|
      PermissionGroupMembership.create(
        permission_group_name: pg,
        member_type: 'user_type',
        member_id: user.id,
      )
      # admins also have all normal user permissions
      PermissionGroupMembership.create(
        permission_group_name: pg,
        member_type: 'user_type',
        member_id: admin.id,
      )
    end

    admin_permission_groups.each do |pg|
      PermissionGroupMembership.create(
        permission_group_name: pg,
        member_type: 'user_type',
        member_id: admin.id,
      )
    end

  end
end
