class CreatePermissionGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :permission_group_membership do |t|
      t.string :permission_group_name, null: false
      t.string :member_type, null: false
      t.integer :member_id, null: false
    end

    create_table :user_type do |t|
      t.string :str_id
    end
    UserType.create(:str_id => 'admin')
    ut = UserType.create(:str_id => 'user')

    add_column :person, :user_type_id, :integer
  end
end
