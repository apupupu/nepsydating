import React, { useEffect, useState } from "react";

import { Button } from "../../../components/button";
import { Form, FormSubmit } from "../../../components/form";
import { Loading } from "../../../components/loading";


export const QuestionForm = (props) => {
  const {
    question = null,
    disabled = false,
    onCancel,
    reload,
  } = props;

  const [data, setData] = useState(null);
  const [collapsed, setCollapsed] = useState(true);

  const loadFormData = () => {
    jQuery.ajax({
      url: '/questions/form_data',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  useEffect(loadFormData, []);

  if (!data) return <Loading />;

  const onSuccess = (data) => {
    if (onCancel) onCancel();
    if (!question) {
      jQuery('form#new-question-form').trigger('reset');
    }
    if (reload) reload();
  }

  const submitPath = question ? '/questions/update' : '/questions/create';
  const canCollapse = !!question && disabled;

  return (
    <Form
      id={question ? `question-form-${question.id}` : 'new-question-form'}
      action={submitPath}
      onSuccess={onSuccess}
      >
      <input type='hidden' name='id' value={question?.id} />
      <div className="row">
        <div className="col col-8">
          Question
          <input type="text" name="name" disabled={disabled} defaultValue={question?.name} />
        </div>
        {canCollapse
        ? <div className="col col-4">
            <Button onClick={() => setCollapsed(!collapsed)}>
              {collapsed ? 'Expand' : 'Collapse'}
            </Button>
          </div>
        : null}
      </div>
      {collapsed && canCollapse ? null
      : <>
        <div className="row">
          <div className="col col-12">
            Type
            <select name="question_type" disabled={disabled} defaultValue={question?.question_type} >
              {data.types.map(t => (
                <option key={t} value={t}>{t}</option>
              ))}
            </select>
          </div>
        </div>
        <div className="row">
          <div className={`col col-${canCollapse ? 6 : 12}`}>
            Question set
            <select name="question_set_id" disabled={disabled} defaultValue={question?.question_set_id} >
              <option>none selected</option>
              {data.question_sets.map(qs => (
                <option key={qs.id} value={qs.id}>{qs.name}</option>
              ))}
            </select>
          </div>
          <div className={`col col-${canCollapse ? 6 : 12}`}>
            Sort order
            <input
              type="number"
              name="sort_order"
              disabled={disabled}
              defaultValue={question?.sort_order}
              title="Questions with a bigger sort order are listed lower by default."
            />
          </div>
          <div className="col col-12">
            Hard fact
            <input
              type="checkbox"
              name="hard_fact"
              disabled={disabled}
              defaultChecked={question?.hard_fact}
              title="Users may not assign a certainty to their answers if the question is about a hard fact."
            />
          </div>
          <div className="col col-12">
            Can private
            <input
              type="checkbox"
              name="can_private"
              disabled={disabled}
              defaultChecked={question?.can_private}
              title="Users can mark the answer as private. Private answers are not shown to anyone else."
            />
          </div>
          <div className="col col-12">
            Can restrict
            <input
              type="checkbox"
              name="can_restrict"
              disabled={disabled}
              defaultChecked={question?.can_restrict}
              title="Users can mark the answer as restricted. Restricted answers are only shown to people they've liked."
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-12">
            Notice
            <textarea name="notice" disabled={disabled} defaultValue={question?.notice} />
          </div>
        </div>
      </>}
      {onCancel
      ? <Button className='btn btn-secondary me-2' onClick={onCancel}>Cancel</Button>
      : null}
      {disabled ? null : <FormSubmit>{question ? 'Update' : 'Create'}</FormSubmit>}
    </Form>
  )
}

