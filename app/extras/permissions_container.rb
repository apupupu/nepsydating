require 'rexml/document'

class PermissionsContainer

  include REXML


  def initialize(person_id=nil, user_type_id=nil)
    @permissions = {}

    permission_groups = []
    unless user_type_id.nil?
      permission_groups += PermissionGroupMembership.where(member_type: 'user_type', member_id: user_type_id).map{|pgm| pgm.permission_group_name}
      permission_groups += PermissionGroupMembership.where(member_type: 'person', member_id: person_id).map{|pgm| pgm.permission_group_name}
    end

    xmlfile = File.open("#{Rails.root}/config/permissions.xml")
    xmldoc = Document.new(xmlfile)

    xmldoc.elements.each("permissions/permissiongroup") do |element|
      if element.attributes['public'] || permission_groups.include?(element.attributes['name'])
        element.elements.each do |p|
          @permissions[p.attributes['code']] = true
        end
      end
    end
  end


  def has_permission?(permission_code)
    return @permissions[permission_code]
  end


  def serialize_permissions()
    return @permissions.keys
  end

end