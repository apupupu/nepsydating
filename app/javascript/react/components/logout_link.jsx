import React from "react";

export const LogoutLink = (props) => {
  const logout = (evt) => {
    evt.preventDefault();
    jQuery.ajax({
      url: '/logout',
      type: 'POST',
      dataType: 'json',
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content'));
      },
      success: (data) => {
        if (data.success) location.reload();
      },
    });
  };
  return <a href="/logout" onClick={logout}>Logout</a>
}
