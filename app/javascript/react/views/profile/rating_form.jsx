import React, { useEffect, useState } from "react";

import { Button } from "../../components/button";
import { Loading } from "../../components/loading";
import { QuestionRatingModal } from "./question_rating_modal";


export const RatingForm = (props) => {
  const [data, setData] = useState(null);
  const [ratings, setRatings] = useState(null);
  const [ratingQuestionId, setRatingQuestionId] = useState(null);
  const [extraData, setExtraData] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: '/questions/list',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  const reloadRatings = () => {
    jQuery.ajax({
      url: '/profile/get_own_ratings',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      const ratingsByQuestionId = {};
      data.ratings.forEach(r => {
        ratingsByQuestionId[r.question_id] ||= [];
        ratingsByQuestionId[r.question_id].push(r);
      });
      setRatings(ratingsByQuestionId);
      setExtraData({ weight_options: data.weight_options });
    });
  }

  useEffect(reload, []);
  useEffect(reloadRatings, []);

  if (!data || !ratings) return <Loading />;

  return <>
    <h2>My preferences</h2>
    <div className="questions-list" style={{ maxWidth: '1200px' }}>
      {data.questions.map(q => {
        if (q.question_type === 'text') return null;
        return <div key={q.id} className="row question mt-2">
          <div className="col-3">
            <span>{q.name}</span>
          </div>
          <div className="col-3">
            <Button onClick={() => setRatingQuestionId(q.id)} >Edit my ratings</Button>
          </div>
        </div>;
      })}
    </div>
    {ratingQuestionId
    ? <QuestionRatingModal
        question={data.questions.filter(q => q.id === ratingQuestionId)[0]}
        ratings={ratings[ratingQuestionId]}
        confidence_options={data.confidence_options}
        weight_options={extraData.weight_options}
        onClose={() => setRatingQuestionId(null)}
        reload={reloadRatings}
      />
    : null}
  </>
}

