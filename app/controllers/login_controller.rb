class LoginController < ApplicationController

  def login
    ls = LoginService.new
    ip_address = request.remote_ip
    person_id = ls.login(params[:username_or_email], params[:password], ip_address)

    if person_id
      session[:logged_person_id] = person_id
      return render(:json => { :success => true })
    else
      return render(:json => { :success => false }, :status => 401)
    end
  rescue
    return render(:json => { :success => false }, :status => 401)
  end

  
  def register
    ls = LoginService.new
    ls.register(params[:name], params[:username], params[:email], params[:password])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def logout
    reset_session()
    return render(:json => { :success => true })
  end

end
