class ApplicationController < ActionController::Base

  before_action :check_permission
  before_action :set_page_vars


  def logged_person_id
    session[:logged_person_id] || nil
  end


  def logged_user_type_id
    return nil if session[:logged_person_id].nil?
    if !session[:logged_user_type_id]
      p = Person.find(logged_person_id)
      session[:logged_user_type_id] = UserType.find(p.user_type_id).id
    end
    return session[:logged_user_type_id]
  end


  def has_permission?(permission_code)
    pc = get_permissions_container()
    pc.has_permission?(permission_code)
  end


  def serialize_permissions
    Rails.cache.read("permissions_container_#{logged_person_id}").serialize_permissions()
  end


  def set_page_vars
    @serialized_permissions = serialize_permissions()
    @logged_in = !logged_person_id.nil?
  end


  private


  def check_permission
    if !has_permission?("#{params["controller"]}.#{params[:action]}")
      return redirect_to('/permission_denied')
    end
  rescue
    reset_session()
    return redirect_to('/login')
  end


  def get_permissions_container()
    cache_key = "permissions_container_#{logged_person_id}"
    pc = Rails.cache.read(cache_key) rescue nil
    if pc.nil?
      pc = PermissionsContainer.new(logged_person_id, logged_user_type_id)
      Rails.cache.write(cache_key, pc)
    end
    return pc
  end

end
