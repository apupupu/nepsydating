class ModerationTicketService

  STATUS = {
    :open => 'open',
    :assigned => 'assigned',
    :resolved => 'resolved',
    :closed => 'closed',
    :reopened => 'reopened',
  }


  def get_form_data()
    q = <<-SQL
      select id, name
      from moderation_ticket_type mtt
      where deleted = false
      order by sort_order asc
    SQL
    mtts = ModerationTicketType.find_by_sql(q)

    return {
      :moderation_ticket_types => mtts,
    }
  end


  def create(person_id, title, body, moderation_ticket_type_id)
    ModerationTicket.create(
      :created_by => person_id,
      :title => title,
      :body => body,
      :moderation_ticket_type_id => moderation_ticket_type_id,
    )
  end


  def list_own(person_id)
    q = <<-SQL
      select
        mt.id,
        mt.created_at,
        mt.title,
        mt.body,
        mtt.name as category_name,
        coalesce(mta.status, '#{STATUS[:open]}') as status,
        mta.description as action_description
      from moderation_ticket mt
      left join moderation_ticket_type mtt
        on mtt.id = mt.moderation_ticket_type_id
      left join moderation_ticket_action mta
        on mta.moderation_ticket_id = mt.id
        and mta.current = true
      where mt.created_by = :person_id
      order by created_at desc
    SQL
    mts = ModerationTicket.find_by_sql([q, :person_id => person_id])

    return {
      :tickets => mts,
    }
  end


  def list()
    q = <<-SQL
      select
        mt.id,
        mt.created_at,
        p.name as author_name,
        mt.title,
        mt.body,
        mtt.name as category_name,
        coalesce(mta.status, '#{STATUS[:open]}') as status,
        mta.assignee_id as assignee_id,
        mta.description as action_description
      from moderation_ticket mt
      left join moderation_ticket_type mtt
        on mtt.id = mt.moderation_ticket_type_id
      left join moderation_ticket_action mta
        on mta.moderation_ticket_id = mt.id
        and mta.current = true
      left join person p
        on p.id = mt.created_by
      order by created_at desc
    SQL
    mts = ModerationTicket.find_by_sql([q])

    return {
      :tickets => mts,
    }
  end


  def get_action_form_data()
    assignee_q = <<-SQL
      select p.id, p.name
      from person p
      inner join user_type ut on ut.id = p.user_type_id
        and ut.str_id = 'admin'
      where deleted = false
    SQL

    return {
      :assignees => Person.find_by_sql(assignee_q),
      :statuses => STATUS.values,
    }
  end


  def create_action(person_id, moderation_ticket_id, assignee_id, status, description, mod_note)
    ModerationTicketAction.transaction do
      ModerationTicketAction.where(:moderation_ticket_id => moderation_ticket_id, :current => true).update_all(:current => false)
      ModerationTicketAction.create(
        :moderation_ticket_id => moderation_ticket_id,
        :created_by => person_id,
        :assignee_id => assignee_id,
        :status => status,
        :description => description,
        :mod_note => mod_note,
        :current => true,
      )
    end
  end


end