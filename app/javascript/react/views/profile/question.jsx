import React, { useState } from "react";

import { Select } from '../../components/select';
import { Button } from "../../components/button";


export const Question = (props) => {
  const {
    question,
    answer,
    confidence_options,
    disabled,
  } = props;
  const isTextAnswer = question.question_type === 'text';
  const isMulti = question.is_multi;
  const firstAnswer = isMulti ? answer?.[0] : answer;
  const [showExplanation, setShowExplanation] = useState(!isTextAnswer && firstAnswer?.text?.length > 0);

  const domain = `answers[${question.id}]`;

  const answerOptionIds = isMulti ? answer?.map(a => a.option_id) ?? [] : null;
  const optionsForSelect = isMulti ? question.options?.map(o => ({ value: o.id, label: o.name })) : null;

  return <>
    <div className="col-3">
      <span>{question.name}</span>
      {question.notice
      ? <>
        <br/>
        <span className="faded">{question.notice}</span>
      </>
      : null}
    </div>
    <div className="col-5">
      {question.question_type === 'select' || question.question_type === 'number_select'
      ? <select name={`${domain}[option_id]`} defaultValue={answer?.option_id} disabled={disabled}>
          <option>not answered</option>
          {question.options?.map(o => {
            return (
              <option key={o.id} value={o.id}>{o.name}</option>
            );
          })}
        </select>
      : null}
      {question.question_type === 'multi_select'
      ? <Select
          name={`${domain}[option_id][]`}
          defaultValue={optionsForSelect?.filter(o => answerOptionIds.indexOf(o.value) >= 0)}
          options={optionsForSelect ?? []}
          isMulti={true}
          isClearable={false}
          disabled={disabled}
        />
      : null}
      {question.question_type === 'radio'
      ? question.options?.map(o => {
          return <React.Fragment key={o.id}>
            <input
              type='radio'
              name={`${domain}[option_id]`}
              value={o.id}
              defaultChecked={o.id === answer?.option_id}
              disabled={disabled}
            />
            {o.name}
          </React.Fragment>;
        })
      : null}
      {question.question_type === 'multi_checkbox'
      ? question.options?.map(o => {
          const optionAnswer = answer?.filter(a => a.option_id === o.id)?.[0];
          return <div key={o.id}>
            <input
              type='checkbox'
              name={`${domain}[option_id][]`}
              value={o.id}
              defaultChecked={answerOptionIds.indexOf(o.id) >= 0}
              disabled={disabled}
            />
            {o.name}
            {!question.hard_fact && !(disabled && !optionAnswer?.confidence)
            ? <select
                name={`${domain}[confidence][${o.id}]`}
                defaultValue={optionAnswer?.confidence ?? 'normal'}
                disabled={disabled}
                >
                {confidence_options.map(c => {
                  return (
                    <option key={c} value={c}>{I18n.t(`confidence_short_${c}`)}</option>
                  );
                })}
              </select>
            : null}
          </div>;
        })
      : null}
      {isTextAnswer || showExplanation
      ? <textarea
          name={`${domain}[text]`}
          defaultValue={firstAnswer?.text}
          disabled={disabled}
        />
      : null}
    </div>
    <div className="col-2">
      {!question.hard_fact && question.question_type !== 'multi_checkbox'
      ? <select
          name={`${domain}[confidence]`}
          defaultValue={firstAnswer?.confidence ?? 'normal'}
          disabled={disabled}
          >
          {confidence_options.map(c => {
            return (
              <option key={c} value={c}>{I18n.t(`confidence_short_${c}`)}</option>
            );
          })}
        </select>
      : null}
    </div>
    <div className="col-2 stacked">
      {question.can_restrict && !disabled
      ? <span title="Restricted answers are only shown to people you've liked.">
          <input
            type='checkbox'
            name={`${domain}[restricted]`}
            defaultChecked={firstAnswer?.restricted}
            className='me-1'
          />
          Restricted
        </span>
      : null}
      {question.can_private && !disabled
      ? <span title="Private answers are only visible to yourself. They'll still be used to calculate compatibility scores.">
          <input
            type='checkbox'
            name={`${domain}[privated]`}
            defaultChecked={firstAnswer?.privated}
            className='me-1'
          />
          Private
        </span>
      : null}
      {!isTextAnswer && !showExplanation && !disabled
      ? <Button onClick={() => setShowExplanation(true)}>Add explanation</Button>
      : null}
    </div>
  </>;
}
