class ProfileService


  WEIGHT_OPTIONS = [-19, -9, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 9, 19]

  LINK_STATUS = {
    :liked => 'liked',
    :dismissed => 'dismissed',
    :blocked => 'blocked',
  }


  def show(to_person_id, person_id)
    # TODO: only do this if it's outdated
    update_exact_scores(to_person_id, [person_id])

    pq = <<-SQL
      select
        p.name,
        ppl.status,
        ppl.score,
        coalesce(ppl.status = '#{LINK_STATUS[:liked]}' AND ppr.status = '#{LINK_STATUS[:liked]}', false) as is_match,
        coalesce(ppr.status = '#{LINK_STATUS[:liked]}', false) as is_liked,
        coalesce(ppr.status = '#{LINK_STATUS[:blocked]}', false) as is_blocked
      from person p
      left join person_person_link ppl
        on ppl.person_id = :to_person_id
        and ppl.other_person_id = :person_id
      left join person_person_link ppr
        on ppr.person_id = :person_id
        and ppr.other_person_id = :to_person_id
      where p.id = :person_id
        and p.deleted = false
    SQL
    person = Person.find_by_sql([pq, :person_id => person_id, :to_person_id => to_person_id]).first

    q = <<-SQL
      select
        a.id,
        a.question_id,
        q.question_type,
        q.question_type in (:multi_answer_types) as is_multi,
        a.question_option_id as option_id,
        a.text,
        a.confidence
      from question_answer a
      left join question q
        on a.question_id = q.id
      where a.person_id = :person_id
        and q.deleted = false
        and (
          q.can_private = false
          OR (:is_blocked = false AND a.privated = false)
        )
        and (
          q.can_restrict = false
          OR (:is_blocked = false AND (a.restricted = false OR :is_liked))
        )
    SQL
    answers = QuestionAnswer.find_by_sql([q,
      :person_id => person_id,
      :to_person_id => to_person_id,
      :is_liked => person.is_liked,
      :is_blocked => person.is_blocked,
      :multi_answer_types => QuestionService::MULTI_ANSWER_TYPES,
    ])

    # Hide the true values of these fields
    person.is_blocked = false
    person.is_liked = false
    return {
      :answers => answers,
      :person => person,
    }
  end


  def show_own(person_id)
    q = <<-SQL
      select
        a.id,
        a.question_id,
        q.question_type,
        q.question_type in (:multi_answer_types) as is_multi,
        a.question_option_id as option_id,
        a.text,
        a.confidence,
        a.privated,
        a.restricted
      from question_answer a
      left join question q
        on a.question_id = q.id
      where a.person_id = :person_id
    SQL

    answers = QuestionAnswer.find_by_sql([q, {
      :person_id => person_id,
      :multi_answer_types => QuestionService::MULTI_ANSWER_TYPES,
    }])

    return {
      :answers => answers,
    }
  end


  def update_own(person_id, answers)
    QuestionAnswer.transaction do
      answers.each do |question_id, answer_params|
        question = Question.find(question_id)
        raise if question.nil?
        find_params = {
          :person_id => person_id,
          :question_id => question_id,
        }
        is_multi = QuestionService::MULTI_ANSWER_TYPES.include?(question.question_type)
        option_ids = QuestionOption.where(id: answer_params['option_id']).pluck(:id)
        update_params = get_answer_update_params(question, answer_params)
        if is_multi
          QuestionAnswer.where(find_params).delete_all
          option_ids.each do |option_id|
            confidence = QuestionService::MULTI_CONFIDENCE_TYPES.include?(question.question_type) \
              ? QuestionService::CONFIDENCE.values.select{|c| c == answers[question_id]['confidence'][option_id.to_s]}.first \
              : answer_params['confidence']
            QuestionAnswer.create(find_params.merge(update_params).merge({:question_option_id => option_id, :confidence => confidence}))
          end
        else
          option_id = option_ids.first
          text = answer_params['text']
          answer = QuestionAnswer.where(find_params).first
          update_params[:question_option_id] = option_id
          update_params[:text] = text unless text.nil?
          if answer
            answer.update(update_params)
          else
            QuestionAnswer.create(find_params.merge(update_params))
          end
        end
      end
    end
  end


  def update_status(person_id, other_person_id, new_status)
    raise unless LINK_STATUS.values.include?(new_status)
    PersonPersonLink.transaction do
      find_params = {
        :person_id => person_id,
        :other_person_id => other_person_id,
      }
      ppl = PersonPersonLink.where(find_params).first
      ppl = PersonPersonLink.create(find_params) if ppl.nil?
      ppl.update(
        :status => new_status,
      )
    end
  end


  def get_own_ratings(person_id)
    q = <<-SQL
      select *
      from answer_rating r
      where r.person_id = :person_id
      order by r.priority asc
    SQL

    ratings = AnswerRating.find_by_sql([q, :person_id => person_id])

    return {
      :ratings => ratings,
      :weight_options => WEIGHT_OPTIONS,
    }
  end


  def update_own_ratings(person_id, question_id, ratings)
    AnswerRating.transaction do
      question = Question.where(id: question_id, deleted: false).first
      # Delete deleted rows
      ratings = [] if ratings.nil?
      ids = ratings.map{|r| r['id']}
      existing = AnswerRating.where(
        :person_id => person_id,
        :question_id => question.id,
      ).where.not(:id => ids)
      existing.delete_all() unless existing.blank?
      # Create/update the rest
      ratings.each do |r|
        params = {
          :person_id => person_id,
          :question_id => question.id,
          :question_option_id => r['question_option_id'],
          :to_question_option_id => r['to_question_option_id'],
          :confidences => r['confidences'] ? r['confidences'].select{|c| QuestionService::CONFIDENCE.values.include?(c)} : nil,
          :weight => WEIGHT_OPTIONS.include?(r['weight'].to_i) ? r['weight'].to_i : 0,
          :priority => r['priority'] || 0,
        }
        if r['id'].blank?
          AnswerRating.create(params)
        else
          ar = AnswerRating.find(r['id'])
          ar.update(params)
        end
      end
    end
  end


  def list(person_id, link_status)
    if link_status
      q = <<-SQL
        select p.id, p.name, ppl.status, ppl.score
        from person_person_link ppl
        inner join person p
          on p.id = ppl.other_person_id
        where ppl.person_id = :person_id
          and ppl.status = :status
          and p.deleted = false
          and ppl.person_id != ppl.other_person_id
        order by ppl.updated_at desc
      SQL

      people = Person.find_by_sql([q, :status => link_status, :person_id => person_id])
    else
      Person.transaction do
        calculate_match_scores(person_id)
        q = <<-SQL
          select p.id, p.name, ppl.score
          from person_person_link ppl
          inner join person p
            on p.id = ppl.other_person_id
          where ppl.person_id = :person_id
            and ppl.status is null
            and p.deleted = false
          order by ppl.score desc
          limit 1
        SQL

        people = Person.find_by_sql([q, :person_id => person_id])
      end
    end

    return people
  end


  private


  def get_answer_update_params(question, answer_params)
    confidence = question.hard_fact ? nil : QuestionService::CONFIDENCE.values.select{|c| c == answer_params['confidence']}.first
    privated = question.can_private ? answer_params['privated'] == 'on' : false
    restricted = question.can_restrict ? answer_params['restricted'] == 'on' : false
    return {
      :confidence => confidence,
      :text => answer_params['text'],
      :privated => privated,
      :restricted => restricted,
    }
  end


  def calculate_match_scores(person_id)
    last_rating = AnswerRating.where(person_id: person_id).order(updated_at: :desc).first&.updated_at || Time.now

    weights_by_answer = initialise_weights_by_answer(person_id)

    answers_by_weight = {}
    weights_by_answer.each do |answer, weight|
      answers_by_weight[weight] ||= []
      answers_by_weight[weight] << answer
    end

    # TODO: this should be initialised to include the best possible answers for each question
    accepted_answers = []

    q = <<-SQL
      select p.id, ppl.id as link_id, ppl.score, ppl.score_updated_at
      from person p
      left join person_person_link ppl
        on ppl.person_id = :person_id
        and ppl.other_person_id = p.id
      left join (
        select qa.person_id
        from question_answer qa
        where qa.question_option_id is not null
          and concat(qa.question_option_id, '+', coalesce(qa.confidence, 'null'), '+', qa.question_id) not in (:accepted_answers)
        group by qa.person_id
      ) bad_answers
        on bad_answers.person_id = p.id
      where ppl.status is null
        and bad_answers.person_id is null
        and p.id != :person_id
    SQL

    positives = []

    WEIGHT_OPTIONS.reverse_each do |weight|
      # For each weight, get the answers to each question that are rated at that weight or better
      # and find every person who fulfills all of those answers.
      # Keep going (lowering the weight) until at least one person is found
      # then calculate the exact score for those people.
      # This doesn't actually prioritise the best matches, just the 'least dislikable' ones
      next if answers_by_weight[weight].nil?
      accepted_answers += answers_by_weight[weight]
      positives = Person.find_by_sql([q, person_id: person_id, accepted_answers: accepted_answers])
      break if positives.length > 0
    end

    update_exact_scores(person_id, positives.pluck(:id), weights_by_answer)
  end


  def initialise_weights_by_answer(person_id)
    weights_by_answer = {}
    range_questions = Question.where(deleted: false, question_type: QuestionService::QUESTION_TYPE[:number_select]).pluck(:id)

    # initialise weights to zero
    qoq = <<-SQL
      select qo.id, q.hard_fact, q.id as question_id
      from question_option qo
      inner join question q
        on (q.id = qo.question_id OR q.cloned_options_question_id = qo.question_id)
      where qo.deleted = false
        and q.deleted = false
    SQL
    QuestionOption.find_by_sql(qoq).each do |qo|
      (qo.hard_fact ? ['null'] : QuestionService::CONFIDENCE.values).each do |c|
        key = "#{qo.id}+#{c}+#{qo.question_id}"
        weights_by_answer[key] = 0
      end
    end

    # TODO: cache this
    AnswerRating.where(person_id: person_id).order(priority: :asc).each do |rating|
      if range_questions.include?(rating.question_id)
        range = [
          rating.question_option_id.nil? ? -Float::INFINITY : QuestionOption.find(rating.question_option_id).name.to_i,
          rating.to_question_option_id.nil? ? Float::INFINITY : QuestionOption.find(rating.to_question_option_id).name.to_i,
        ]
        QuestionOption.where(question_id: rating.question_id).each do |qo|
          next unless (range.first..range.last).include?(qo.name.to_i)
          get_confidences(rating.confidences).each do |c|
            key = "#{qo.id}+#{c}+#{rating.question_id}"
            weights_by_answer[key] = rating.weight
          end
        end
      else
        option_ids = rating.question_option_id.nil? ? QuestionOption.where(question_id: rating.question_id).pluck(:id) : [rating.question_option_id]
        option_ids.each do |oid|
          get_confidences(rating.confidences).each do |c|
            key = "#{oid}+#{c}+#{rating.question_id}"
            weights_by_answer[key] = rating.weight
          end
        end
      end
    end

    puts weights_by_answer
    return weights_by_answer
  end


  def update_exact_scores(person_id, other_person_ids, weights_by_answer = nil)
    weights_by_answer = initialise_weights_by_answer(person_id) if weights_by_answer.nil?

    other_person_ids.each do |p_id|
      aq = <<-SQL
        select qa.*, concat(qa.question_option_id, '+', coalesce(qa.confidence, 'null'), '+', qa.question_id) as answer, q.name
        from question_answer qa
        inner join question q on qa.question_id = q.id
        where qa.person_id = :person_id
          and qa.question_option_id is not null
      SQL
      answers = QuestionAnswer.find_by_sql([aq, person_id: p_id])
      score = 0
      answers.each do |a|
        puts "#{a.name}, #{a.answer}, #{weights_by_answer[a.answer]}"
        score += weights_by_answer[a.answer]
      end
      puts score

      find_params = {person_id: person_id, other_person_id: p_id}
      ppl = PersonPersonLink.where(find_params).first
      ppl = PersonPersonLink.new(find_params) if ppl.nil?
      ppl.update(
        score: score,
        score_updated_at: Time.now,
        updated_at: ppl.updated_at,
      )
    end
  end


  def get_confidences(confidences)
    return confidences.nil? ? ['null'] : (confidences.empty? ? QuestionService::CONFIDENCE.values : confidences)
  end


end