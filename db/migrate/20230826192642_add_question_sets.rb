class AddQuestionSets < ActiveRecord::Migration[7.0]
  def change
    create_table :question_set do |t|
      t.timestamps
      t.string :name, null: false
      t.integer :sort_order, null: false
      t.boolean :required, null: false
      t.boolean :deleted, null: false, default: false
      t.datetime :deleted_at
    end

    add_column(:question, :question_set_id, :integer)
  end
end
