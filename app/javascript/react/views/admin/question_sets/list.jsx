import React, { useEffect, useState } from "react";

import { Button } from "../../../components/button";
import { Loading } from "../../../components/loading";
import { QuestionSetForm } from "./form";


export const QuestionSetList = (props) => {
  const [data, setData] = useState(null);
  const [editing, setEditing] = useState({});

  const reload = () => {
    jQuery.ajax({
      url: '/question_sets/list',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  useEffect(reload, []);

  if (!data) return <Loading />;

  const editQuestionSet = (qs) => {
    setEditing({ ...editing, [qs.id]: true });
  }

  const stopEditQuestionSet = (qs) => {
    setEditing({ ...editing, [qs.id]: false });
  }

  const deleteQuestionSet = (qs) => {
    if (!confirm(`Really delete question set ${qs.name}?`)) return;
    jQuery.ajax({
      url: '/question_sets/delete',
      type: 'POST',
      dataType: 'json',
      data: { id: q.id },
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content'));
      },
    }).then(data => {
      reload();
    });
  }

  return <>
    <h2>New question set</h2>
    <QuestionSetForm reload={reload} />
    <br />
    <h2>Existing question sets</h2>
    <div className="question-sets-list" style={{ maxWidth: '800px' }}>
      {data.question_sets.map(qs => {
        const editingQS = editing[qs.id];
        return <div key={qs.id} className="row question">
          <QuestionSetForm
            questionSet={qs}
            disabled={!editingQS}
            onCancel={editingQS ? () => stopEditQuestionSet(qs) : null}
          />
          {!editingQS
          ? <div>
              <Button onClick={() => editQuestionSet(qs)}>Edit</Button>
              <Button className='btn btn-danger ms-2' onClick={() => deleteQuestionSet(qs)}>Delete</Button>
            </div>
          : null}
        </div>;
      })}
    </div>
  </>
}

