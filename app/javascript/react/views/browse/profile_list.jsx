import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Loading } from "../../components/loading";
import { ProfileShow } from "../profile/show";


export const ProfileList = (props) => {
  const { status } = props;

  const [data, setData] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: `/profile/browse/${status ?? ''}`,
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  useEffect(reload, [status]);

  if (!data) return <Loading />;

  return status ? (
    data.length > 0
    ? <div className="profile-list">
      {data.map(p => (
        <div key={p.id} className="row">
          <p>
            <Link to={`/browse/show/${p.id}`}>{p.name}</Link>
            <span style={{marginLeft: '2em'}} >({p.score} points)</span>
          </p>
        </div>
      ))}
    </div>
    : <span>No {status} profiles to show</span>
  ) : data.length === 0 ? (
    <span>No more profiles to show :/</span>
  ) : (
    <ProfileShow key={data[0].id} id={data[0].id} afterAction={reload} />
  );
}
