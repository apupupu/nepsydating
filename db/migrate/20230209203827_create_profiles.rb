class CreateProfiles < ActiveRecord::Migration[7.0]
  def change

    create_table :profile do |t|
      t.timestamps
      t.integer :person_id, null: false
    end

    create_table :question do |t|
      t.timestamps
      t.string :name, null: false
      t.string :question_type, null: false
      t.integer :sort_order, null: false
      t.boolean :hard_fact, null: false
      t.boolean :private, null: false
      t.boolean :can_restrict, null: false
      t.boolean :deleted, null: false, default: false
      t.datetime :deleted_at
    end

    create_table :question_option do |t|
      t.timestamps
      t.integer :question_id, null: false
      t.string :name, null: false
      t.integer :sort_order, null: false
      t.boolean :deleted, null: false, default: false
      t.datetime :deleted_at
    end

    create_table :question_answer do |t|
      t.timestamps
      t.integer :person_id, null: false
      t.integer :question_id, null: false
      t.integer :question_option_id
      t.string :text
      t.string :confidence
      t.boolean :restricted, null: false
    end

  end
end
