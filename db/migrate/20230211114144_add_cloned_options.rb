class AddClonedOptions < ActiveRecord::Migration[7.0]
  def change
    drop_table :profile
    add_column :question, :cloned_options_question_id, :integer
  end
end
