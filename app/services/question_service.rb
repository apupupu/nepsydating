class QuestionService


  QUESTION_TYPE = {
    :radio => 'radio',
    :select => 'select',
    :number_select => 'number_select',
    :multi_select => 'multi_select',
    :multi_checkbox => 'multi_checkbox',
    :text => 'text',
  }

  CONFIDENCE = {
    :high => 'high',
    :normal => 'normal',
    :low => 'low',
    :very_low => 'very_low',
  }

  MULTI_ANSWER_TYPES = [
    QUESTION_TYPE[:multi_select],
    QUESTION_TYPE[:multi_checkbox],
  ]
  MULTI_CONFIDENCE_TYPES = [
    QUESTION_TYPE[:multi_checkbox],
  ]


  def form_data()
    return {
      :types => QUESTION_TYPE.values,
      :question_sets => QuestionSet.where(deleted: false).order(sort_order: :asc),
    }
  end


  def list()
    q = <<-SQL
      select
        q.id,
        q.name,
        q.question_type,
        q.question_type in (:multi_answer_types) as is_multi,
        q.question_set_id,
        q.sort_order,
        q.notice,
        q.hard_fact,
        q.can_private,
        q.can_restrict,
        qs.name as set_name,
        q.cloned_options_question_id,
        qo.options
      from question q
      left join (
        select qo.question_id,
          json_agg(json_build_object(
            'id', qo.id,
            'name', qo.name,
            'question_id', qo.question_id
          ) order by qo.sort_order asc) as options
        from question_option qo
        where qo.deleted = false
        group by qo.question_id
      ) qo
        on qo.question_id = q.id OR qo.question_id = q.cloned_options_question_id
      left join question_set qs on qs.id = q.question_set_id and qs.deleted = false
      where q.deleted = false
      order by qs.sort_order asc nulls first, q.sort_order asc
    SQL

    return {
      :questions => Question.find_by_sql([q, :multi_answer_types => MULTI_ANSWER_TYPES]),
      :confidence_options => CONFIDENCE.values,
    }
  end


  def create(params)
    Question.create(params)
  end


  def update(id, params)
    q = Question.find(id)
    q.update(params)
  end


  def delete(id)
    q = Question.find(id)
    q.update(:deleted => true, :deleted_at => Time.now)
  end


  def list_sets()
    q = <<-SQL
      select
        qs.id,
        qs.name,
        qs.sort_order,
        qs.required
      from question_set qs
      where qs.deleted = false
      order by qs.sort_order asc
    SQL

    return {
      :question_sets => QuestionSet.find_by_sql(q),
    }
  end


  def create_set(params)
    QuestionSet.create(params)
  end


  def update_set(id, params)
    qs = QuestionSet.find(id)
    qs.update(params)
  end


  def delete_set(id)
    qs = QuestionSet.find(id)
    qs.update(:deleted => true, :deleted_at => Time.now)
  end


  def update_options(question_id, cloned_options_question_id, options)
    QuestionOption.transaction do
      question = Question.find(question_id)
      if question.cloned_options_question_id != cloned_options_question_id
        question.update(:cloned_options_question_id => cloned_options_question_id)
      end
      # Delete deleted options
      options = [] if options.nil?
      ids = options.map{|o| o['id']}
      QuestionOption.where(:question_id => question_id).where.not(:id => ids).update_all(
        :deleted => true,
        :deleted_at => Time.now,
      )
      # Create/update the rest
      options.each_with_index do |o, i|
        params = {
          :question_id => question_id,
          :name => o['name'],
          :sort_order => i,
        }
        if o['id'].blank?
          qo = QuestionOption.create(params)
        else
          qo = QuestionOption.find(o['id'])
          qo.update(params)
        end
      end
    end
  end


end