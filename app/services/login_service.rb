require 'bcrypt'

class LoginService

  def login(username_or_email, password, ip_address)
    lc = LoginCredentials.where(username: username_or_email).first
    if lc.nil?
      p = Person.where(email: username_or_email, deleted: false).first
      lc = LoginCredentials.where(person_id: p.id).first
    else
      p = Person.where(id: lc.person_id, deleted: false).first
    end

    if p.nil? || lc.nil?
      return nil
    end

    success = BCrypt::Password.new(lc.hashed_password) == password

    LoginAttempt.create(
      :login_credentials_id => lc.id,
      :successful => success,
      :ip_address => ip_address,
    )

    return success ? p.id : nil
  end


  def register(name, username, email, password)
    hashed_password = BCrypt::Password.create(password)
    username = email if username.blank?
    # TODO: feedback if email/username are not unique ??
    Person.transaction do
      ut = UserType.find_by(str_id: 'user')
      p = Person.create(
        name: name,
        email: email.blank? ? nil : email,
        user_type_id: ut.id,
      )
      lc = LoginCredentials.create(
        :person_id => p.id,
        :username => username,
        :hashed_password => hashed_password,
      )
    end
  end

end