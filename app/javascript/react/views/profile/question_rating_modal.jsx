import React, { useState } from "react";

import { Select } from '../../components/select';
import { Modal, ModalClose } from '../../components/modal';
import { Form, FormSubmit } from "../../components/form";
import { randomString } from "../../../helpers/stringHelpers";
import { Button } from "../../components/button";


export const QuestionRatingModal = (props) => {
  const {
    question,
    confidence_options,
    weight_options,
    onClose,
  } = props;

  const [ratings, setRatings] = useState(props.ratings ?? []);

  const formId = `ratings-form-${question.id}`;
  const actions = [<ModalClose key='c' />, <FormSubmit key='s' formId={formId} />];
  const isNumeric = question.question_type === 'number_select';

  const addRating = () => {
    setRatings([...ratings, {
      question_option_id: null,
      to_question_option_id: null,
      weight: 0,
      priority: ratings[ratings.length - 1]?.priority + 1 || 0,
      strId: randomString(8),
    }]);
  }

  const deleteRating = (r) => {
    setRatings(ratings.filter(rr => rr.id !== r.id || rr.strId !== r.strId));
  }

  const onSuccess = () => {
    props.reload();
    onClose();
  }

  return <Modal actions={actions} title={`Ratings: ${question.name}`} onHide={onClose}>
    <Form id={formId} action='/profile/update_own_ratings' onSuccess={onSuccess}>
      <input type="hidden" name="question_id" value={question.id} />
      <Button className='btn btn-primary' onClick={addRating}>Add row</Button>
      <table className="answer-ratings-table">
        <thead>
          {ratings.length > 0
          ? <tr>
            <th>{isNumeric ? 'From' : 'Answer'}</th>
            {isNumeric ? <th>To</th> : null}
            {!question.hard_fact ? <th>Confidences</th> : null}
            <th>Weight</th>
            <th>Priority</th>
            <th>Actions</th>
          </tr>
          : null}
        </thead>
        <tbody>
          {ratings.map(r => (
            <tr key={r.id ?? r.strId}>
              <td>
                <input type="hidden" name="ratings[][id]" value={r.id} />
                <span>
                  <select name="ratings[][question_option_id]" defaultValue={r.question_option_id}>
                    <option value=''>{isNumeric ? '(min)' : '(default)'}</option>
                    {question.options.map(o => (
                      <option key={o.id} value={o.id}>{o.name}</option>
                    ))}
                  </select>
                </span>
              </td>
              {isNumeric
              ? <td>
                <span>
                  <select name="ratings[][to_question_option_id]" defaultValue={r.to_question_option_id}>
                    <option value=''>(max)</option>
                    {question.options.map(o => (
                      <option key={o.id} value={o.id}>{o.name}</option>
                    ))}
                  </select>
                </span>
              </td>
              : null}
              {!question.hard_fact
              ? <td>
                <span>
                  <Select
                    name="ratings[][confidences][]"
                    defaultValue={(r.confidences || []).map(c => ({ value: c, label: I18n.t(`confidence_short_${c}`) }))}
                    options={confidence_options.map(c => ({ value: c, label: I18n.t(`confidence_short_${c}`) }))}
                    isMulti={true}
                    isClearable={false}
                    baseColor='#444'
                    labelColor='#666'
                    placeholder='Any'
                  />
                </span>
              </td>
              : null}
              <td>
                <span>
                  <select name="ratings[][weight]" defaultValue={r.weight}>
                    {weight_options.map(w => (
                      <option key={w} value={w}>{w}</option>
                    ))}
                  </select>
                </span>
              </td>
              <td>
                <span>
                  <input style={{maxWidth: '80px'}} type="number" name="ratings[][priority]" defaultValue={r.priority} />
                </span>
              </td>
              <td>
                <span>
                  <Button className='btn btn-danger' onClick={() => deleteRating(r)}>Delete</Button>
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Form>
  </Modal>;
}
