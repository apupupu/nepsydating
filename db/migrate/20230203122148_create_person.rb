class CreatePerson < ActiveRecord::Migration[7.0]
  def change
    create_table :person do |t|
      t.string :name, limit: 60, null: false
      t.string :email
      t.boolean :deleted, default: false, null: false
      t.timestamps
      t.datetime :deleted_at
    end
    add_index :person, :email, unique: true

    create_table :login_credentials do |t|
      t.integer :person_id, null: false
      t.string :username
      t.string :hashed_password
      t.timestamps
    end
    add_index :login_credentials, :username, unique: true
    add_index :login_credentials, :person_id, unique: true

    create_table :login_attempt do |t|
      t.integer :login_credentials_id
      t.string :ip_address
      t.string :device_name
      t.boolean :successful, null: false
      t.datetime :created_at, null: false
    end
    add_index :login_attempt, :login_credentials_id
  end
end
