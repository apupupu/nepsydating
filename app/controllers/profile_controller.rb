class ProfileController < ApplicationController

  def show
    s = ProfileService.new
    data = s.show(logged_person_id, params[:profile_id])
    question_data = QuestionService.new.list()
    return render(:json => data.merge(question_data))
  end


  def show_own
    s = ProfileService.new
    data = s.show_own(logged_person_id)
    return render(:json => data)
  end


  def update_own
    s = ProfileService.new
    s.update_own(logged_person_id, params[:answers])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def update_status
    s = ProfileService.new
    s.update_status(logged_person_id, params[:profile_id], params[:status])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def get_own_ratings
    s = ProfileService.new
    data = s.get_own_ratings(logged_person_id)
    return render(:json => data)
  end


  def update_own_ratings
    s = ProfileService.new
    s.update_own_ratings(logged_person_id, params[:question_id], params[:ratings])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def list
    s = ProfileService.new
    data = s.list(logged_person_id, params[:status])
    return render(:json => data)
  end


end
