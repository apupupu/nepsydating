import React from "react";
import ReactSelect from "react-select";


export const Select = (props) => {
  const {
    baseColor = '#333',
    labelColor = '#555',
    labelHoverColor = '#888',
    indicatorColor = '#777',
    indicatorColor2 = '#aaa',
    deleteColor = '#f00',
    optionColor = '#557',
    ...rest
  } = props;
  return (
    <ReactSelect
      className='react-select'
      styles={{
        control: (base, state) => ({...base, backgroundColor: baseColor, borderRadius: 0}),
        menu: (base, state) => ({...base, backgroundColor: baseColor}),
        option: (base, state) => ({...base, backgroundColor: state.isFocused ? optionColor : baseColor}),
        multiValueLabel: (base, state) => ({
          ...base,
          color: 'white',
          backgroundColor: labelColor,
        }),
        multiValueRemove: (base, state) => ({
          ...base,
          backgroundColor: labelColor,
          ':hover': { backgroundColor: labelHoverColor, color: deleteColor },
        }),
        multiValue: (base, state) => ({...base, backgroundColor: labelColor}),
        dropdownIndicator: (base, state) => ({
          ...base,
          color: state.isFocused ? labelColor : indicatorColor,
          ':hover': { color: state.isFocused ? labelHoverColor : indicatorColor2 },
        }),
        clearIndicator: (base, state) => ({
          ...base,
          color: state.isFocused ? labelColor : indicatorColor,
          ':hover': { color: deleteColor },
        }),
      }}
      isDisabled={props.disabled}
      {...rest}
    />
  )
}
