class CreatePersonPersonLink < ActiveRecord::Migration[7.0]
  def change
    create_table :person_person_link do |t|
      t.timestamps
      t.integer :person_id, null: false
      t.integer :other_person_id, null: false
      t.string :status
      t.integer :score
      t.datetime :score_updated_at
    end
    add_index :person_person_link, [:person_id, :other_person_id], unique: true
  end
end
