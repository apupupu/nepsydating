import React, { useEffect, useState } from "react";
import { format } from "date-fns";

import { Loading } from "../../components/loading";
import { ActionModal } from "./action_modal";


export const ModerationTicketList = (props) => {
  const { admin } = props;
  const [data, setData] = useState(null);
  const [modalShown, setModalShown] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: admin ? '/moderation_ticket/list' : '/moderation_ticket/list_own',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  useEffect(reload, []);


  if (!data) return <Loading />;

  return (
    <div className="moderation-ticket-list">
      {modalShown
      ? <ActionModal
        ticket={modalShown}
        hide={() => setModalShown(null)}
        reload={reload}
        />
      : null}
      {data.tickets.map(o => (
        <div key={o.id} className="row moderation-ticket">
          <p>
            <span>{o.category_name}</span>
            <span style={{marginLeft: '2em'}} >({o.status})</span>
            <span className="float-right">Created at {format(new Date(o.created_at), 'yyyy-MM-dd HH:mm:ss')}</span>
          </p>
          {admin ? <p>
            <span>#{o.id}</span>
            <span className="float-right">Author: {o.author_name}</span>
          </p> : null}
          <h3>{o.title}</h3>
          <p>{o.body}</p>
          {admin ? <div>
            <button onClick={() => setModalShown(o)} >Action</button>
          </div> : null}
        </div>
      ))}
    </div>
  )
}

