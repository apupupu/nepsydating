import React from "react";
import { Routes, Route, Link } from "react-router-dom";
import { LogoutLink } from "../components/logout_link";

import { FrontPage } from "../views/login/front_page";
import { Login } from "../views/login/login";
import { Register } from "../views/login/register";


export const LoggedOut = (props) => {
  return <>
    <div className="navbar-links">
      <Link to='/'>Front page</Link>
      <Link to='/login'>Login</Link>
      <Link to='/register'>Register</Link>
    </div>
    <Routes>
      <Route path='/' element={<FrontPage />} />
      <Route path='/login' element={<Login />} />
      <Route path='/register' element={<Register />} />
    </Routes>
  </>;
}
