class CreateAnswerRatings < ActiveRecord::Migration[7.0]
  def change
    create_table :answer_rating do |t|
      t.timestamps
      t.integer :person_id, null: false
      t.integer :question_id, null: false
      t.integer :question_option_id
      t.integer :to_question_option_id
      t.integer :weight, null: false
      t.json :confidences
      t.integer :priority, null: false
    end
  end
end
