import React from "react";
import { Routes, Route, Link } from "react-router-dom";

import { ProfileForm } from "../views/profile/form";
import { RatingForm } from "../views/profile/rating_form";


export const Profile = (props) => {
  return <>
    <div>
      <ul>
        <li>
          <Link to='/profile'>Own profile</Link>
        </li>
        <li>
          <Link to='/profile/preferences'>Own preferences</Link>
        </li>
      </ul>
    </div>
    <div className="main-content profile">
      <Routes>
        {User.hasPermission('profile.show_own') ? <Route path='/' element={<ProfileForm />} /> : null}
        {User.hasPermission('profile.get_own_ratings') ? <Route path='/preferences' element={<RatingForm />} /> : null}
      </Routes>
    </div>
  </>;
}
