class CreateMessage < ActiveRecord::Migration[7.0]
  def change
    create_table :message do |t|
      t.timestamps
      t.integer :sent_by
      t.integer :recipient, null: false
      t.text :body, null: false
      t.boolean :deleted, null: false, default: false
      t.datetime :deleted_at
      t.datetime :read_at
    end
  end
end
