class CreateModerationTicket < ActiveRecord::Migration[7.0]
  def change
    create_table :moderation_ticket_type do |t|
      t.timestamps
      t.string :str_id, null: false, limit: 30
      t.string :name, null: false
      t.integer :sort_order
      t.boolean :deleted, null: false, default: false
      t.datetime :deleted_at
    end
    ModerationTicketType.create(
      :str_id => 'spam_report',
      :name => 'Report spam',
      :sort_order => 0
    )
    ModerationTicketType.create(
      :str_id => 'abuse_report',
      :name => 'Report abuse of the system',
      :sort_order => 1
    )
    ModerationTicketType.create(
      :str_id => 'threat_report',
      :name => 'Report a threat of injury, assault or death',
      :sort_order => 10
    )
    ModerationTicketType.create(
      :str_id => 'account_issue',
      :name => 'Account issue',
      :sort_order => 20
    )
    ModerationTicketType.create(
      :str_id => 'bug_report',
      :name => 'Bug report',
      :sort_order => 40
    )
    ModerationTicketType.create(
      :str_id => 'feature_request',
      :name => 'Feature request',
      :sort_order => 41
    )
    ModerationTicketType.create(
      :str_id => 'feedback',
      :name => 'General feedback',
      :sort_order => 50
    )
    ModerationTicketType.create(
      :str_id => 'gdpr_request',
      :name => 'GDPR request',
      :sort_order => 90
    )
    ModerationTicketType.create(
      :str_id => 'other',
      :name => 'Other',
      :sort_order => 666
    )


    create_table :moderation_ticket do |t|
      t.timestamps
      t.integer :created_by, null: false
      t.string :title, null: false
      t.text :body, null: false
      t.integer :moderation_ticket_type_id, null: false
    end

    create_table :moderation_ticket_action do |t|
      t.integer :moderation_ticket_id, null: false
      t.datetime :created_at, null: false
      t.integer :created_by, null: false
      t.integer :assignee_id
      t.string :status, null: false
      t.text :description
      t.text :mod_note
      t.boolean :current, null: false
    end
  end
end
