import React, { useEffect, useState } from "react";

import { Button } from "../../components/button";
import { Form, FormSubmit } from "../../components/form";
import { Loading } from "../../components/loading";
import { Question } from "./question";


export const ProfileForm = (props) => {
  const [data, setData] = useState(null);
  const [answers, setAnswers] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: '/questions/list',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  const reloadAnswers = () => {
    jQuery.ajax({
      url: '/profile/show_own',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      const answersByQuestionId = {};
      data.answers.forEach(a => {
        if (a.is_multi) {
          answersByQuestionId[a.question_id] ||= [];
          answersByQuestionId[a.question_id].push(a);
        } else {
          answersByQuestionId[a.question_id] = a;
        }
      });
      setAnswers(answersByQuestionId);
    });
  }

  useEffect(reload, []);
  useEffect(reloadAnswers, []);

  if (!data || !answers) return <Loading />;
  let setName = null;

  return <>
    <h2>My profile</h2>
    <Form action='/profile/update_own'>
      <div className="questions-list" style={{ maxWidth: '1200px' }}>
        {data.questions.map(q => {
          const showSetName = setName !== q.set_name ? q.set_name : null;
          setName = q.set_name;
          return <React.Fragment key={q.id}>
            {showSetName
            ? <>
                <hr/>
                <h4>{q.set_name}</h4>
              </>
            : null}
            <div className="row question mt-3">
              <Question
                question={q}
                answer={answers[q.id]}
                confidence_options={data.confidence_options}
              />
            </div>
          </React.Fragment>;
        })}
      </div>
      <FormSubmit>Save</FormSubmit>
    </Form>
  </>
}

