class ChangePrivateToCanPrivate < ActiveRecord::Migration[7.0]
  def change
    rename_column(:question, :private, :can_private)

    add_column(:question_answer, :privated, :boolean, null: false, default: false)
    change_column_default(:question_answer, :privated, from: false, to: nil)

    q = <<-SQL
      select qa.*, q.can_private
      from question_answer qa
      inner join question q on q.id = qa.question_id
    SQL
    QuestionAnswer.find_by_sql(q).each do |qa|
      qa.update(:privated => true) if qa.can_private
    end
  end
end
