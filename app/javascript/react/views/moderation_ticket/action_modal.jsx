import React, { useEffect, useState } from "react";

import { Modal, ModalClose } from "../../components/modal";
import { Form, FormSubmit } from "../../components/form";
import { Loading } from "../../components/loading";


export const ActionModal = (props) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    jQuery.ajax({
      url: '/moderation_ticket/action_form_data',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }, []);

  const onSuccess = (data) => {
    if (data.success) {
      props.reload();
      props.hide();
    }
  }

  const formId = 'ticket-action-form';

  const actions = [<ModalClose key='c' />, <FormSubmit key='s' formId={formId} />];

  return (
    <Modal
      onHide={props.hide}
      title={`#${props.ticket.id}: ${props.ticket.title}`}
      actions={actions}
      >
      {!data ? <Loading />
      : <Form id={formId} action='/moderation_ticket/process' onSuccess={onSuccess}>
        <input type="hidden" name="moderation_ticket_id" value={props.ticket.id} />
        <div className="row">
          Assignee:
          <select name="assignee" defaultValue={props.ticket.assignee_id}>
            <option>(unassigned)</option>
            {data.assignees.map(o => (
              <option key={o.id} value={o.id}>{o.name}</option>
            ))}
          </select>
        </div>
        <div className="row">
          New status:
          <select name="status" defaultValue={props.ticket.status}>
            {data.statuses.map(s => (
              <option key={s} value={s}>{s}</option>
            ))}
          </select>
        </div>
        <div className="row">
          Description (visible to reporter):
          <textarea name="description" rows="5" cols="50" ></textarea>
        </div>
        <div className="row">
          Mod note:
          <textarea name="mod_note" rows="5" cols="50" ></textarea>
        </div>
      </Form>}
    </Modal>
  );
}
