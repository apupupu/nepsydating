class AddNoticeToQuestion < ActiveRecord::Migration[7.0]
  def change
    add_column :question, :notice, :text
  end
end
