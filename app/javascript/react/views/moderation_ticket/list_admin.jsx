import React from "react";

import { ModerationTicketList } from "./list";


export const ModerationTicketListAdmin = (props) => {
  return (
    <ModerationTicketList admin={true} />
  );
}

