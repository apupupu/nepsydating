class QuestionController < ApplicationController


  def list
    s = QuestionService.new
    data = s.list()
    return render(:json => data)
  end


  def form_data
    s = QuestionService.new
    data = s.form_data()
    return render(:json => data)
  end


  def create
    s = QuestionService.new
    s.create(question_params())
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def update
    s = QuestionService.new
    s.update(params[:id], question_params())
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def delete
    s = QuestionService.new
    s.delete(params[:id])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def update_options
    s = QuestionService.new
    s.update_options(params[:question_id], params[:cloned_options_question_id], params[:options])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def list_sets
    s = QuestionService.new
    data = s.list_sets()
    return render(:json => data)
  end


  def create_set
    s = QuestionService.new
    s.create_set(question_set_params())
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def update_set
    s = QuestionService.new
    s.update_set(params[:id], question_set_params())
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  def delete_set
    s = QuestionService.new
    s.delete_set(params[:id])
    return render(:json => { :success => true })
  rescue
    return render(:json => { :success => false }, :status => 400)
  end


  private


  def question_params
    return {
      :name => params[:name],
      :question_type => params[:question_type],
      :question_set_id => params[:question_set_id],
      :sort_order => params[:sort_order],
      :hard_fact => params[:hard_fact] == 'on',
      :can_private => params[:can_private] == 'on',
      :can_restrict => params[:can_restrict] == 'on',
      :notice => params[:notice],
    }
  end


  def question_set_params
    return {
      :name => params[:name],
      :sort_order => params[:sort_order],
      :required => params[:required] == 'on',
    }
  end


end
