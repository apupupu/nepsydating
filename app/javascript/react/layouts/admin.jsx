import React from "react";
import { Routes, Route, Link } from "react-router-dom";

import { ModerationTicketListAdmin } from "../views/moderation_ticket/list_admin";
import { QuestionList } from "../views/admin/questions/list";
import { QuestionSetList } from "../views/admin/question_sets/list";


export const Admin = (props) => {
  return <>
    <div>
      <ul>
        <li>
          <Link to='/admin/tickets/list'>Moderation Tickets</Link>
        </li>
        <li>
          <Link to='/admin/questions/list'>Profile Questions</Link>
        </li>
      </ul>
    </div>
    <div className="main-content admin">
      <Routes>
        {User.hasPermission('question.list') ? <Route path='/questions/list' element={<QuestionList />} /> : null}
        {User.hasPermission('question.list_sets') ? <Route path='/question_sets/list' element={<QuestionSetList />} /> : null}
        {User.hasPermission('moderation_ticket.list') ? <Route path='/tickets/list' element={<ModerationTicketListAdmin />} /> : null}
      </Routes>
    </div>
  </>;
}
