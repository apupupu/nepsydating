import React, { useState } from "react";

import { Modal, ModalClose } from "../../../components/modal";
import { Form, FormSubmit } from "../../../components/form";
import { Button } from "../../../components/button";
import { randomString } from "../../../../helpers/stringHelpers";


export const OptionsModal = (props) => {
  const {
    question,
    questions,
  } = props;

  const [options, setOptions] = useState(question.options || []);

  const onSuccess = (data) => {
    if (data.success) {
      props.reload();
      props.hide();
    }
  }

  const addOption = () => {
    setOptions([...options, { name: '', strId: randomString(8) }]);
  }

  const autoAddOptions = () => {
    try {
      const string = prompt('Enter range in format start,end(,step):') || '';
      const parts = string.split(',');
      const start = +parts[0];
      const end = +parts[1];
      const min = Math.min(start, end);
      const max = Math.max(start, end);
      const step = parts.length > 2 ? +parts[2] : 1;
      const newOptions = [...options];
      for (let i = start; i <= max && i >= min; i += step) {
        newOptions.push({ name: i, strId: randomString(8) });
      }
      setOptions(newOptions);
    } catch(e) {
      console.error(e);
    }
  }

  const quickAddOptions = () => {
    try {
      const string = prompt('Enter comma-separated list:') || '';
      const parts = string.split(',');
      const newOptions = [...options];
      parts.forEach(p => {
        newOptions.push({ name: p, strId: randomString(8) });
      });
      setOptions(newOptions);
    } catch(e) {
      console.error(e);
    }
  }

  const deleteOption = (o) => {
    setOptions(options.filter(oo => oo.id !== o.id || oo.strId !== o.strId));
  }

  const moveOption = (index, newIndex) => {
    const newOptions = [...options];
    newOptions[index] = options[newIndex];
    newOptions[newIndex] = options[index];
    setOptions(newOptions);
  }

  const quickMove = (oldIndex) => {
    try {
      const newIndex = +(prompt('Enter new index:'));
      if (newIndex >= 0 && newIndex < options.length) {
        moveOption(oldIndex, newIndex);
      }
    } catch(e) {
      console.error(e);
    }
  }

  const formId = 'question-options-form';
  const actions = [<ModalClose key='c' />, <FormSubmit key='s' formId={formId} />];
  const isNumeric = question.question_type === 'number_select';

  return (
    <Modal
      onHide={props.hide}
      title={`Options: ${question.name}`}
      actions={actions}
      >
      <Form id={formId} action='/questions/save_options' onSuccess={onSuccess}>
        <input type="hidden" name="question_id" value={question.id} />
        <div className="question-options-list">
          <Button className='btn btn-primary' onClick={addOption}>Add row</Button>
          {isNumeric ? <Button className='btn btn-secondary ms-2' onClick={autoAddOptions}>Add range</Button> : null}
          {!isNumeric ? <Button className='btn btn-secondary ms-2' onClick={quickAddOptions}>Add list</Button> : null}
          <span className="ms-2">
            Use options from:
            <select name="cloned_options_question_id" defaultValue={question.cloned_options_question_id}>
              <option value={null}>-</option>
              {questions.map(q => (q.id === question.id || (isNumeric && q.question_type !== 'number_select')) ? null : (
                <option key={q.id} value={q.id}>{q.name}</option>
              ))}
            </select>
          </span>
          {options.map((o, i) => {
            if (o.question_id && o.question_id !== question.id) return null;
            return (
              <div className="row question-option-row" key={o.id || o.strId}>
                <input type="hidden" name="options[][id]" value={o.id} />
                <div className="col-6">
                  <span onClick={() => quickMove(i)} >{i}.</span>
                  <input type={isNumeric ? 'number' : 'text'} name="options[][name]" defaultValue={o.name} />
                </div>
                <div className="col-4">
                  {i > 0 ? <Button onClick={() => moveOption(i, i-1)} >Move up</Button> : null}
                  {i < options.length - 1 ? <Button onClick={() => moveOption(i, i+1)} >Move down</Button> : null}
                </div>
                <div className="col-2">
                  <Button className='btn btn-danger' onClick={() => deleteOption(o)} >Delete</Button>
                </div>
              </div>
            )
          })}
        </div>
      </Form>
    </Modal>
  );
}
