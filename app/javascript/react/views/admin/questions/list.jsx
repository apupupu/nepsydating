import React, { useEffect, useState } from "react";

import { Button } from "../../../components/button";
import { Loading } from "../../../components/loading";
import { QuestionForm } from "./form";
import { OptionsModal } from "./options_modal";
import { Link } from "react-router-dom";


export const QuestionList = (props) => {
  const [data, setData] = useState(null);
  const [editingQuestions, setEditingQuestions] = useState({});
  const [editingOptions, setEditingOptions] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: '/questions/list',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }

  useEffect(reload, []);

  if (!data) return <Loading />;

  const editQuestion = (q) => {
    setEditingQuestions({ ...editingQuestions, [q.id]: true });
  }

  const stopEditQuestion = (q) => {
    setEditingQuestions({ ...editingQuestions, [q.id]: false });
  }

  const deleteQuestion = (q) => {
    if (!confirm(`Really delete question ${q.name}?`)) return;
    jQuery.ajax({
      url: '/questions/delete',
      type: 'POST',
      dataType: 'json',
      data: { id: q.id },
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content'));
      },
    }).then(data => {
      reload();
    });
  }

  const editOptions = (q) => {
    setEditingOptions(q.id);
  }

  let setName = null;

  return <>
    <Link to='../question_sets/list'>Manage question sets</Link>
    <h2>New question</h2>
    <QuestionForm reload={reload} />
    <br />
    <h2>Existing questions</h2>
    <div className="questions-list" style={{ maxWidth: '800px' }}>
      {data.questions.map(q => {
        const editing = editingQuestions[q.id];
        const showSetName = setName !== q.set_name ? q.set_name : null;
        setName = q.set_name;
        return <React.Fragment key={q.id}>
          {showSetName
          ? <>
              <hr/>
              <h4>{q.set_name}</h4>
            </>
          : null}
          <div key={q.id} className="row question">
            <QuestionForm
              question={q}
              disabled={!editing}
              onCancel={editing ? () => stopEditQuestion(q) : null}
              />
            {!editing
            ? <div>
                <Button onClick={() => editQuestion(q)}>Edit</Button>
                <Button className='btn btn-danger ms-2' onClick={() => deleteQuestion(q)}>Delete</Button>
                <Button className='btn btn-primary ms-2' onClick={() => editOptions(q)}>Edit Options</Button>
              </div>
            : null}
          </div>
        </React.Fragment>;
      })}
    </div>
    {editingOptions
    ? <OptionsModal
        question={data.questions.filter(q => q.id === editingOptions)[0]}
        hide={() => setEditingOptions(null)}
        reload={reload}
        questions={data.questions}
      />
    : null}
  </>
}

