class MessagingController < ApplicationController


  def create
    s = MessagingService.new
    success = params[:body].length > 0 && s.create(logged_person_id, params[:recipient_id], params[:body])
    return render(:json => success)
  end


  def list
    s = MessagingService.new
    data = s.list(logged_person_id, params[:person_id])
    return render(:json => data)
  end


  def summary
    s = MessagingService.new
    data = s.summary(logged_person_id)
    return render(:json => data)
  end


end
