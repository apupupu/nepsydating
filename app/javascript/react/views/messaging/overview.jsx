import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import { dateToStr } from "../../../helpers/timeHelpers";
import { Loading } from "../../components/loading";


export const MessagingOverview = (props) => {
  const [chats, setChats] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: `/messages/summary`,
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setChats(data);
    });
  }

  useEffect(reload, []);

  if (chats === null) return <Loading />;

  return <>
    <h2>Your chats</h2>
    <ul>
      {chats.map(c => {
        const timestamp = <span className="me-1">[{dateToStr(new Date(c.last_message_at))}]</span>;
        const author = <strong className="me-1">{c.last_sender_name}:</strong>;
        return <li key={c.other_person_id}>
          <Link to={`/browse/show/${c.other_person_id}`}>
            {c.name}
            {timestamp}
            {author}
            {c.last_message_body}
          </Link>
        </li>;
      })}
    </ul>
  </>;
}
