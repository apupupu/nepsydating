import React from "react";


export const Button = (props) => {
  const {
    children,
    onClick,
    className,
    ...rest
  } = props;

  return (
    <button
      className={className || "btn btn-secondary"}
      type="button"
      onClick={onClick}
      {...rest}
      >
      {children}
    </button>
  );
}
