import React from "react";
import { Form, FormSubmit } from "./form";

export const LoginForm = (props) => {
  const onSuccess = (data) => {
    if (data.success) location.href = '/';
  }

  return <Form action='/login' id='login-form' onSuccess={onSuccess}>
    Username or email
    <input type='text' name="username_or_email" />
    Password
    <input type='password' name='password' />
    <FormSubmit>Login</FormSubmit>
  </Form>
}
