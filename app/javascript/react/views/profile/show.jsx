import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { Button } from "../../components/button";
import { Loading } from "../../components/loading";
import { Messaging } from "../../components/messaging";
import { Question } from "./question";


export const ProfileShow = (props) => {
  const params = useParams();
  const id = params.id ?? props.id;

  const [data, setData] = useState(null);
  const [answers, setAnswers] = useState(null);

  const reload = () => {
    jQuery.ajax({
      url: `/profile/show/${id}`,
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      const answersByQuestionId = {};
      data.answers.forEach(a => {
        if (a.is_multi) {
          answersByQuestionId[a.question_id] ||= [];
          answersByQuestionId[a.question_id].push(a);
        } else {
          answersByQuestionId[a.question_id] = a;
        }
      });
      setData(data);
      setAnswers(answersByQuestionId);
    });
    if (props.afterAction) props.afterAction();
  }

  const updateStatus = (status) => {
    if (status === 'blocked' && !confirm('Really block this person?')) return;
    jQuery.ajax({
      url: '/profile/update_status',
      type: 'POST',
      dataType: 'json',
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content'));
      },
      data: { status: status, profile_id: id },
    }).then(data => {
      if (data.success) reload();
    }).catch(data => {
      console.error(data);
    });
  }

  useEffect(reload, []);

  if (!data || !answers) return <Loading />;
  let setName = null;

  return <>
    <h2>{data.person.name} ({data.person.score} points) {data.person.score === 69 ? '(nice!)' : null}</h2>
    {data.person.is_match ? <span>You like each other!</span> : <span>You've {data.person.status || 'not interacted with'} this person</span>}
    <Button className='btn btn-primary ms-2' onClick={() => updateStatus('liked')} >Like</Button>
    <Button className='btn btn-secondary ms-2' onClick={() => updateStatus('dismissed')} >Dismiss</Button>
    <Button className='btn btn-secondary ms-2' onClick={() => updateStatus('blocked')} >Block</Button>
    <div className="questions-list" style={{ maxWidth: '1200px' }}>
      {data.questions.map(q => {
        if (!answers[q.id]) return null;
        const showSetName = setName !== q.set_name ? q.set_name : null;
        setName = q.set_name;

        return <React.Fragment key={q.id}>
          {showSetName
          ? <>
              <hr/>
              <h4>{q.set_name}</h4>
            </>
          : null}
          <div className="row question mt-3">
            <Question
              question={q}
              answer={answers[q.id]}
              disabled={true}
              confidence_options={data.confidence_options}
            />
          </div>
        </React.Fragment>;
      })}
    </div>
    <Messaging other_person_id={id} />
  </>
}

