import React, { useEffect, useState } from "react";


export const Loading = (props) => {
  const [phase, setPhase] = useState(0);

  const tick = () => {
    if (phase === 3) {
      setPhase(0);
    } else {
      setPhase(phase + 1);
    }
  }

  useEffect(() => {
    const t = setInterval(tick, 800);
    return () => clearInterval(t);
  }, []);

  return <span>
    Loading{'.'.repeat(phase)}
  </span>
}
