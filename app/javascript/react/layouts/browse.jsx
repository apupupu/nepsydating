import React from "react";
import { Routes, Route, Link } from "react-router-dom";

import { ProfileList } from "../views/browse/profile_list";
import { ProfileShow } from "../views/profile/show";


export const Browse = (props) => {
  return <>
    <div>
      <ul>
        <li>
          <Link to='/browse'>Browse</Link>
        </li>
        <li>
          <Link to='/browse/liked'>View liked profiles</Link>
        </li>
        <li>
          <Link to='/browse/dismissed'>View dismissed profiles</Link>
        </li>
        <li>
          <Link to='/browse/blocked'>View blocked profiles</Link>
        </li>
      </ul>
    </div>
    <div className="main-content browse">
      <Routes>
        {User.hasPermission('profile.show') ? <Route path='/show/:id' element={<ProfileShow />} /> : null}
        {User.hasPermission('profile.list') ? <Route path='/' element={<ProfileList />} /> : null}
        {User.hasPermission('profile.list') ? <Route path='/liked' element={<ProfileList status='liked' />} /> : null}
        {User.hasPermission('profile.list') ? <Route path='/dismissed' element={<ProfileList status='dismissed' />} /> : null}
        {User.hasPermission('profile.list') ? <Route path='/blocked' element={<ProfileList status='blocked' />} /> : null}
      </Routes>
    </div>
  </>;
}
