import React, { useEffect, useState } from "react";

import { Form, FormSubmit } from "../../components/form";
import { Loading } from "../../components/loading";
import { Link } from "react-router-dom";


export const ModerationTicketForm = (props) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    jQuery.ajax({
      url: '/moderation_ticket/form_data',
      type: 'GET',
      dataType: 'json',
    }).then(data => {
      setData(data);
    });
  }, []);

  const onSuccess = (data) => {
    if (data.success) {
      location.href = '/mod_request/list_own';
    }
  }

  return (
    <div className="moderation-ticket-form">
      <Link to='/mod_request/list_own'>View my previously filed tickets</Link>
      <h2>File a ticket to the admins</h2>
      {!data ? <Loading />
      : <Form onSuccess={onSuccess} action='/moderation_ticket/create'>
        <div className="row">
          Category: 
          <select name="category">
            <option>-- Please select --</option>
            {data.moderation_ticket_types.map(o => (
              <option value={o.id} key={o.id} >{o.name}</option>
            ))}
          </select>
        </div>
        <div className="row">
          Title: 
          <input type="text" name="title" />
        </div>
        <div className="row">
          Body: 
          <textarea name="body" cols="100" rows="10" />
        </div>
        <FormSubmit />
      </Form>}
    </div>
  );
}

