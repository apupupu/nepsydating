import React from "react";
import { Routes, Route, Link } from "react-router-dom";
import { LogoutLink } from "../components/logout_link";

import { FrontPage } from "../views/login/front_page";
import { ModerationTicketForm } from "../views/moderation_ticket/form";
import { ModerationTicketListOwn } from "../views/moderation_ticket/list_own";
import { Admin } from "./admin";
import { Browse } from "./browse";
import { Profile } from "./profile";


export const LoggedIn = (props) => {
  return <>
    <div className="navbar-links">
      <Link to='/'>Front page</Link>
      <Link to='/browse'>Browse</Link>
      <Link to='/profile'>Profile</Link>
      {User.hasPermission('view_admin_dashboard')
      ? <Link to='/admin'>Admin</Link>
      : <Link to='/mod_request'>Moderation</Link>
      }
      <LogoutLink/>
    </div>
    <Routes>
      <Route path='/' element={<FrontPage />} />
      <Route path='/profile/*' element={<Profile />} />
      <Route path='/browse/*' element={<Browse />} />
      <Route path='/mod_request' element={<ModerationTicketForm />} />
      <Route path='/mod_request/list_own' element={<ModerationTicketListOwn />} />
      {User.hasPermission('view_admin_dashboard') ? <Route path='/admin/*' element={<Admin />} /> : null}
    </Routes>
  </>;
}
