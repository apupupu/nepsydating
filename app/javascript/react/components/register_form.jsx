import React from "react";
import { Form } from "./form";

export const RegisterForm = (props) => {
  const onSuccess = (data) => {
    if (data.success) location.href = '/login';
  }

  return <Form action='/register' id='register-form' onSuccess={onSuccess}>
    <div className="row">
      Name (public)
      <input type='text' name="name" />
    </div>
    <div className="row">
      Email (not public)
      <input type='email' name="email" />
    </div>
    <div className="row">
      Username (optional, not public)
      <input type='text' name="username" />
    </div>
    <div className="row">
      Password
      <input type='password' name='password' />
    </div>
    <div className="row">
      Repeat password (TODO)
      <input type='password' name='confirm_password' />
    </div>
    <div className="row">
      <button type="submit">Register</button>
    </div>
  </Form>
}
